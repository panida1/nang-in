# Nang-In

ระบบเช่าหนังสือของร้านนางอินทร์

## Feature

1. สามารถเพิ่มหนังสือที่ให้เช่าได้ โดยใส่ ชื่อหนังสือ, ราคาหนังสือ, จำนวนเล่มที่มีให้เช่า
2. ดูหนังสือทั้งหมดที่มีให้เช่า
3. เช่าหนังสือ โดยระบุ ชื่อหนังสือ, ข้อมูลผู้เช่า (ชื่อ, อายุ)
4. คืนหนังสือ โดยระบุ ชื่อหนังสือ, ข้อมูลผู้เช่า (ชื่อ, อายุ)
5. ดูว่าใครยังไม่ได้คืนหนังสือเล่มใดบ้าง
6. ดูหนังสือที่ถูกยืมมากที่สุด (จะไม่ขึ้นชื่อหนังสือที่ไม่มีคนยืมเลย)

### Output

```bash
$ npm run dev

> add_book The_Martian 270 5
Added "The_Martian" to store.

> add_book Executional 65 10
Added "Executional" to store.

> add_book Yosuga_no_Sora 45 3
Added "Yosuga_no_Sora" to store.

> list_books
The_Martian, Executional, Yosuga_no_Sora

> rent The_Martian Aruk 23
Aruk rent The_Martian

> rent The_Martian Siwat 19
Siwat rent The_Martian

> rent Yosuga_no_Sora Intouch 17
Intouch rent Yosuga_no_Sora

> rent Yosuga_no_Sora Thanakorn 19
Thanakorn rent Yosuga_no_Sora

> rent Yosuga_no_Sora Kongka 19
Kongka rent Yosuga_no_Sora

> rent Yosuga_no_Sora Aruk 23
Out of stock

> return Yosuga_no_Sora Intouch 17
Intouch return Yosuga_no_Sora

> return Yosuga_no_Sora Kongka 19
Kongka return Yosuga_no_Sora

> rent Yosuga_no_Sora Aruk 23
Aruk rent Yosuga_no_Sora

> return Yosuga_no_Sora Aruk 23
Aruk return Yosuga_no_Sora

> list_unreturned_histories
Aruk is not return The_Martian
Siwat is not return The_Martian
Thanakorn is not return Yosuga_no_Sora

> list_popular_books
Yosuga_no_Sora, The_Martian
```
