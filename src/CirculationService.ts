import { LendingManager } from "./manager/LendingManager";
import { Lending } from "./Lending";
import { BookManager } from "./manager/BookManager";
import { Book } from "./Book";
import { Guest } from "./Guest";

export class CirculationService {
  constructor(
    public bookManager = new BookManager(),
    public lendingManager = new LendingManager()
  ) {}

  public addNewBooks(title: string, price: string, numOfBooks: string): void {
    this.bookManager.add(
      new Book(title, parseInt(price), parseInt(numOfBooks))
    );
  }

  public listBooks(): Book[] {
    const uniqBooks = [...new Set(this.bookManager.books)];

    return uniqBooks;
  }

  public rent(title: string, guest: Guest): Lending {
    const book = this.bookManager.findByname(title) as Book;

    if (!book) throw new Error(`Book not found`);
    if (book.numOfBooks === 0) throw new Error(`Out of stock`);

    const lending = this.lendingManager.createLending(guest, book);
    this.lendingManager.add(lending);
    this.bookManager.decreaseAmount(book);
    this.bookManager.increaseTime(book);

    return lending;
  }

  public return(title: string, guest: Guest): Book {
    const book = this.bookManager.findByname(title) as Book;

    if (!book) throw new Error(`Book not found`);

    const lending = this.lendingManager.findByGuestAndTitle(title, guest);
    this.lendingManager.remove(lending);
    this.bookManager.increaseAmount(book);

    return book;
  }

  public listUnreturnGuest(): Lending | Lending[] {
    return this.lendingManager.listUnreturned();
  }

  public listPopularBook(): Book | Book[] {
    return this.bookManager.listPopularBook();
  }
}
