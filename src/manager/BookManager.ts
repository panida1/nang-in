import { Book } from "../Book";

export class BookManager {
  constructor(public books: Book[] = []) {}

  public add(book: Book): Book[] {
    this.books.push(book);

    return this.books;
  }

  public decreaseAmount(book: Book) {
    --book.numOfBooks;
  }
  public increaseAmount(book: Book) {
    ++book.numOfBooks;
  }

  public increaseTime(book: Book) {
    ++book.time;
  }

  public listPopularBook(): Book | Book[] {
    const books = this.books.filter(book => book.time > 0);

    if (!books) throw new Error(`No poplular book`);
    return books;
  }

  public findByname(title: string): Book | undefined {
    return this.books.find(book => book.title === title);
  }
}
