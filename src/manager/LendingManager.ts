import { Lending } from "../Lending";
import { Guest } from "../Guest";
import { Book } from "../book";

export class LendingManager {
  constructor(public lendings: Lending[] = []) {}

  public createLending(guest: Guest, book: Book): Lending {
    return new Lending(guest, book);
  }

  public add(lending: Lending): Lending[] {
    this.lendings.push(lending);

    return this.lendings;
  }

  public remove(targetLending: Lending): Lending[] {
    this.lendings = this.lendings.filter(
      lending =>
        !(
          lending.guest.name === targetLending.guest.name &&
          lending.book.title === targetLending.book.title
        )
    );

    return this.lendings;
  }

  public findByGuestAndTitle(title: string, guest: Guest): Lending {
    const lending = this.lendings.find(
      lending =>
        lending.guest.name === guest.name && lending.book.title === title
    );

    if (!lending) throw new Error(`Lending not find`);
    return lending;
  }

  public listUnreturned(): Lending | Lending[] {
    if (this.lendings === []) throw new Error(`Nothing`);

    return this.lendings;
  }
}
