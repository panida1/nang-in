import { Guest } from "../Guest";

export class GuestManager {
  constructor(public guests: Guest[] = []) {}
}
