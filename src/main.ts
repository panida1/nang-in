import { Lending } from "./Lending";
import { readFileSync } from "fs";
import { CirculationService } from "./CirculationService";
import { Guest } from "./Guest";
import { Book } from "./book";

class Command {
  constructor(public name: string, public params: string[]) {}
}

function main() {
  const filePath = "input.txt";
  const commands = getCommandFromInputFile(filePath);
  const circulationService = new CirculationService();

  commands.forEach(command => {
    logCommand(command);

    switch (command.name) {
      case "add_book": {
        const [title, price, numberOfBooks] = command.params;

        circulationService.addNewBooks(title, price, numberOfBooks);

        console.log(`Added "${title}" to store.`);
        break;
      }
      case "list_books": {
        const books = circulationService.listBooks();

        const bookNames = books.map(book => book.title);

        console.log(bookNames.join(`, `));
        break;
      }
      case "rent": {
        const [title, name, age] = command.params;

        try {
          circulationService.rent(title, new Guest(name, parseInt(age)));

          console.log(`${name} rent ${title}`);
        } catch (error) {
          console.log(error.message);
        }
        break;
      }
      case "return": {
        const [title, name, age] = command.params;

        try {
          circulationService.return(title, new Guest(name, parseInt(age)));

          console.log(`${name} return ${title}`);
        } catch (error) {
          console.log(error.message);
        }
        break;
      }
      case "list_unreturned_histories": {
        try {
          const lendings = circulationService.listUnreturnGuest();

          (lendings as Lending[]).forEach(lending => {
            console.log(
              `${lending.guest.name} is not return ${lending.book.title}`
            );
          });
        } catch (error) {
          console.log(error);
        }

        break;
      }
      case "list_popular_books": {
        try {
          const books = circulationService.listPopularBook();

          const bookName = (books as Book[]).map(book => book.title);

          console.log(bookName.join(", "));
        } catch (error) {
          console.log(error);
        }
        break;
      }
      default:
        return;
    }

    console.log("");
  });
}

main();

function getCommandFromInputFile(inputFile: string): Command[] {
  return readFileSync(inputFile, "utf-8")
    .split("\n")
    .map(line => line.split(" "))
    .map(([name, ...params]) => new Command(name, params));
}

function logCommand(command: Command) {
  console.log(`> ${command.name} ${command.params.join(" ")}`);
}
