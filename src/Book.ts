export class Book {
  constructor(
    public title: string,
    public price: number,
    public numOfBooks: number,
    public time: number = 0
  ) {}
}
