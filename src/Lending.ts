import { Guest } from "./Guest";
import { Book } from "./Book";

export class Lending {
  constructor(public guest: Guest, public book: Book) {}
}
