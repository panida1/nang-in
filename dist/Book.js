"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Book {
    constructor(title, price, numOfBooks, time = 0) {
        this.title = title;
        this.price = price;
        this.numOfBooks = numOfBooks;
        this.time = time;
    }
}
exports.Book = Book;
