"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class GuestManager {
    constructor(guests = []) {
        this.guests = guests;
    }
}
exports.GuestManager = GuestManager;
