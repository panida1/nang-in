"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Lending_1 = require("../Lending");
class LendingManager {
    constructor(lendings = []) {
        this.lendings = lendings;
    }
    createLending(guest, book) {
        return new Lending_1.Lending(guest, book);
    }
    add(lending) {
        this.lendings.push(lending);
        return this.lendings;
    }
    remove(targetLending) {
        this.lendings = this.lendings.filter(lending => !(lending.guest.name === targetLending.guest.name &&
            lending.book.title === targetLending.book.title));
        return this.lendings;
    }
    findByGuestAndTitle(title, guest) {
        const lending = this.lendings.find(lending => lending.guest.name === guest.name && lending.book.title === title);
        if (!lending)
            throw new Error(`Lending not find`);
        return lending;
    }
    listUnreturned() {
        if (this.lendings === [])
            throw new Error(`Nothing`);
        return this.lendings;
    }
}
exports.LendingManager = LendingManager;
