"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BookManager {
    constructor(books = []) {
        this.books = books;
    }
    add(book) {
        this.books.push(book);
        return this.books;
    }
    decreaseAmount(book) {
        --book.numOfBooks;
    }
    increaseAmount(book) {
        ++book.numOfBooks;
    }
    increaseTime(book) {
        ++book.time;
    }
    listPopularBook() {
        const books = this.books.filter(book => book.time > 0);
        if (!books)
            throw new Error(`No poplular book`);
        return books;
    }
    findByname(title) {
        return this.books.find(book => book.title === title);
    }
}
exports.BookManager = BookManager;
