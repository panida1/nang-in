"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const LendingManager_1 = require("./manager/LendingManager");
const BookManager_1 = require("./manager/BookManager");
const Book_1 = require("./Book");
class CirculationService {
    constructor(bookManager = new BookManager_1.BookManager(), lendingManager = new LendingManager_1.LendingManager()) {
        this.bookManager = bookManager;
        this.lendingManager = lendingManager;
    }
    addNewBooks(title, price, numOfBooks) {
        this.bookManager.add(new Book_1.Book(title, parseInt(price), parseInt(numOfBooks)));
    }
    listBooks() {
        const uniqBooks = [...new Set(this.bookManager.books)];
        return uniqBooks;
    }
    rent(title, guest) {
        const book = this.bookManager.findByname(title);
        if (!book)
            throw new Error(`Book not found`);
        if (book.numOfBooks === 0)
            throw new Error(`Out of stock`);
        const lending = this.lendingManager.createLending(guest, book);
        this.lendingManager.add(lending);
        this.bookManager.decreaseAmount(book);
        this.bookManager.increaseTime(book);
        return lending;
    }
    return(title, guest) {
        const book = this.bookManager.findByname(title);
        if (!book)
            throw new Error(`Book not found`);
        const lending = this.lendingManager.findByGuestAndTitle(title, guest);
        this.lendingManager.remove(lending);
        this.bookManager.increaseAmount(book);
        return book;
    }
    listUnreturnGuest() {
        return this.lendingManager.listUnreturned();
    }
    listPopularBook() {
        return this.bookManager.listPopularBook();
    }
}
exports.CirculationService = CirculationService;
